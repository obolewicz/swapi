//
//  PeopleViewModel.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import Moya
import SVProgressHUD

class PeopleViewModel {
    var exampleText = Variable("")
    var examplePerson = Variable(Person())
    
    var receivedLastPage = false
    var query = Variable("")
    var page = Variable(1)
    
    let disposebag = DisposeBag()
    var items = Variable([Person]())
    
    init() {
        self.setupPageObserver()
    }
    
    func setupPageObserver() {
        self.page.asObservable().subscribe(onNext: {
            print("<> Next Page",$0)
            self.getItems()
        }).disposed(by: self.disposebag)
    }
    
    func getFirstPage() {
        self.items.value.removeAll()
        self.page.value = 1
    }
    
    func getItems() {
        SVProgressHUD.show()
        let provider = MoyaProvider<SwapiService>()
        let requestObservable = provider.rx.request(SwapiService.people(page: self.page.value, search: self.query.value)).debug().mapArray(to: Person.self, keyPath: "results")
        let resultMap = requestObservable.subscribe(onSuccess: { (peopleArray) in
            if let people = peopleArray {
                self.items.value.append(contentsOf: people)
            }
            else {
                self.receivedLastPage = true
            }
            SVProgressHUD.dismiss()
        }) { (error) in
            print("<> ", error)
            SVProgressHUD.dismiss()
        }
        self.disposebag.insert(resultMap)
    }
    
    func itemsCount() -> Int {
        return self.items.value.count
    }
    func item(at index: Int) -> Person? {
        return self.items.value[safe: index]
    }
    func nextPage() {
        if !self.receivedLastPage {
            self.page.value+=1
        }
    }
}
