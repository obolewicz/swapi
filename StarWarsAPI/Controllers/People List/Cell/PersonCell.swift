//
//  PersonCell.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import UIKit

class PersonCell: UITableViewCell {
    static let identifier = "PersonCell"
    static let height:CGFloat = 50
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var mass: UILabel!
    
    class func register(to tableView: UITableView) {
        let cellNib = UINib(nibName: "PersonCell", bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: PersonCell.identifier)
    }
    
    func setup(person: Person) {
        self.name.text = person.name
        self.height.text = person.height
        self.mass.text = person.mass
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

