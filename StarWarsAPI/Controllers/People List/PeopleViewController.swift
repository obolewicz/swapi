//
//  ViewController.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 14.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Moya

class PeopleViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    let viewModel = PeopleViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupVariableObserver()
        self.setupTableView()
    }
    
    func setupVariableObserver() {
        self.viewModel.items.asObservable().subscribe(onNext: {
            print("<> New Pople",$0)
            self.refreshData()
        }).disposed(by: self.viewModel.disposebag)
    }
    
    func refreshData() {
        self.tableView.reloadData()
    }
    
    func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorInset = .zero
        self.tableView.estimatedRowHeight = PersonCell.height
        self.tableView.rowHeight = UITableViewAutomaticDimension
        PersonCell.register(to: self.tableView)
    }
}

extension PeopleViewController: UITableViewDelegate {
    //commet for auto cell size
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PersonCell.height
    }
}

extension PeopleViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.viewModel.itemsCount()-1 {
            self.viewModel.nextPage()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.itemsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: PersonCell.identifier) as? PersonCell {
            if let item = self.viewModel.item(at: indexPath.row) {
                cell.setup(person: item)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
        
    }
}

