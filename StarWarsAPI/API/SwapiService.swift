//
//  SwapiService.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import Foundation
import Moya

enum SwapiService {
    case people(page: Int, search: String)
}

extension SwapiService: TargetType {
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
    var baseURL: URL { return URL(string: "https://swapi.co/api")! }
    var path: String {
        switch self {
        case .people:
            return "/people"
            
        }
    }
    var method: Moya.Method {
        return .get
    }
    var sampleData: Data {
        switch self {
        case .people:
            return "{{\"id\": \"1\", \"language\": \"Swift\", \"url\": \"https://api.github.com/repos/mjacko/Router\", \"name\": \"Router\"}}}".data(using: .utf8)!
        }
    }
    var task: Task {
        switch self {
        case let .people(page, search):
            return .requestParameters(parameters: ["page":page, "search": search], encoding: URLEncoding.default)
        }
    }
}
