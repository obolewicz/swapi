//
//  DateFormatterHelper.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import Foundation

public class DateFormatterHelper {
    private let onlyDate = "dd MMMM yyyy"
    private let thisYearDate = "dd MMMM"
    private let onlyMonth = "MMMM"
    private let dateIsoFormat = "yyyy-MM-dd' 'HH:mm:ss"
    private let dateBirthFormat = "yyyy-MM-dd"
    private let onlyTimeFormat = "HH:mm:ss"
    
    lazy var formatter: DateFormatter = {
        return DateFormatter()
    }()
    
    public func conversationDate(date: Date) -> String {
        let calendar = Calendar.current
        if date.isLastHour() {
            return date.minutesAgo() + " min ago"
        }
        if calendar.isDateInToday(date) {
            return date.hoursAgo()
        }
        if calendar.isDateInYesterday(date) {
            return "yesterday"
        }
        if date.isLast7Days() {
            return date.daysAgo()
        }
        if calendar.isDateThisYear(date: date) {
            return stringToOnlyDate(date: date)
        }
        return stringToDateWithYear(date: date)
    }
    
    
    public func stringToOnlyDate(date: Date) -> String {
        formatter.dateFormat = self.thisYearDate
        return formatter.string(from: date)
    }
    
    public func stringToDateWithYear(date: Date) -> String {
        formatter.dateFormat = self.onlyDate
        return formatter.string(from: date)
    }
    
    public func dateToTime(date: Date) -> String {
        formatter.dateFormat = self.onlyTimeFormat
        return formatter.string(from: date)
    }
    
    public func stringToDate(date: String) -> Date? {
        formatter.dateFormat = self.dateIsoFormat
        return formatter.date(from: date)
    }
    
    public func dateToString(date: Date) -> String {
        formatter.dateFormat = self.dateIsoFormat
        return formatter.string(from: date)
    }
    
    public func birthStringToDate(date: String) -> Date? {
        formatter.dateFormat = self.dateBirthFormat
        return formatter.date(from: date)
    }
    
    public func birthDateToString(date: Date) -> String {
        formatter.dateFormat = self.dateBirthFormat
        return formatter.string(from: date)
    }
}

