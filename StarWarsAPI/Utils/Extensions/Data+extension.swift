//
//  Data+extension.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import Foundation

extension Data {
    public func json() -> [String: Any]? {
        do {
            if let jsonDict = try JSONSerialization.jsonObject(with: self, options: []) as? [String: Any] {
                return jsonDict
            }
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
    public func json(keyPath: String) -> [String:Any]? {
        if let dataJSON = self.json() {
            if let jsonObject = dataJSON[keyPath] as? [String : Any] {
                return jsonObject
            }
        }
        return nil
    }
    
    public func jsonArray(keyPath: String) -> [[String:Any]]? {
        if let dataJSON = self.json() {
            if let jsonArray = dataJSON[keyPath] as? [[String : Any]] {
                return jsonArray
            }
        }
        return nil
    }
}
