//
//  ObservableType+extension.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import ObjectMapper

extension ObservableType where E == Response {
    
    public func map<T: Mappable>(to type: T.Type, keyPath: String) -> Observable<T?> {
        return flatMap { response -> Observable<T?> in
            if let json = response.data.json(keyPath: keyPath) {
                let object = Mapper<T>().map(JSON: json)
                return Observable.just(object)
            }
            return Observable.just(nil)
        }
    }
    
    public func mapArray<T: Mappable>(to type: T.Type, keyPath: String) -> Observable<Array<T>?> {
        return flatMap { response -> Observable<Array<T>?> in
            if let jsonArray = response.data.jsonArray(keyPath: keyPath) {
                let objects = Mapper<T>().mapArray(JSONArray: jsonArray)
                return Observable.just(objects)
            }
            return Observable.just(nil)
        }
    }
}
