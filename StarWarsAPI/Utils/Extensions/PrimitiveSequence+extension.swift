//
//  PrimitiveSequence+extension.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import ObjectMapper

extension PrimitiveSequence where TraitType == SingleTrait, ElementType == Response {
    
    public func map<T: Mappable>(to type: T.Type, keyPath: String) -> Single<T?> {
        return observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMap { response -> Single<T?> in
                if let json = response.data.json(keyPath: keyPath) {
                    let object = Mapper<T>().map(JSON: json)
                    return Single.just(object)
                }
                return Single.just(nil)
            }
            .observeOn(MainScheduler.instance)
    }
    
    public func mapArray<T: Mappable>(to type: T.Type, keyPath: String) -> Single<[T]?> {
        return observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMap { response -> Single<[T]?> in
                if let jsonArray = response.data.jsonArray(keyPath: keyPath) {
                    let objects = Mapper<T>().mapArray(JSONArray: jsonArray)
                    return Single.just(objects)
                }
                return Single.just(nil)
            }
            .observeOn(MainScheduler.instance)
    }
    
}
