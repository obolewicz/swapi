//
//  Date+extension.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import Foundation

extension Date {
    func monthName() -> String {
        if let month = Calendar.current.dateComponents([.month], from: self).month {
            if let monthName = DateFormatter().monthSymbols[safe: (month-1) % 12] {
                let year = Calendar.current.component(.year, from: self)
                return monthName + " " + String(year)
            }
        }
        return ""
    }
    
    public func isLastHour() -> Bool {
        if let earlyDate = Calendar.current.date(byAdding: .hour, value: -1, to: Date()) {
            return self > earlyDate
        }
        return false
    }
    
    public func isLast7Days() -> Bool {
        if let earlyDate = Calendar.current.date(byAdding: .day, value: -7, to: Date()) {
            return self > earlyDate
        }
        return false
    }
    
    public func sameYear(as date: Date) -> Bool {
        let calendar = NSCalendar.current
        let selfYear = calendar.component(.year, from: self)
        let dateYear = calendar.component(.year, from: date)
        return selfYear == dateYear
    }
    
    
    public func yearString() -> String {
        let calendar = NSCalendar.current
        let year = calendar.component(.year, from: self)
        return String(year)
    }
    
    public func minutesAgo() -> String {
        let sec = Date().timeIntervalSince1970 - self.timeIntervalSince1970
        let min = Int(sec/60)
        if min == 0 {
            return ">1"
        }
        return String(min)
    }
    
    public func hoursAgo() -> String {
        let sec = Date().timeIntervalSince1970 - self.timeIntervalSince1970
        let hours = Int(sec/60/60)
        if hours == 0 {
            return ">1 " + "hour ago"
        }
        else if hours == 1 {
            return "an hour ago"
        }
        return String(hours) + " hours ago"
    }
    
    public func daysAgo() -> String {
        let sec = Date().dayBeginDate().timeIntervalSince1970 - self.timeIntervalSince1970
        let days = Int(sec/60/60/24)
        if days == 0 {
            return ">1 " + "day ago"
        }
        else if days == 1 {
            return "a day ago"
        }
        return String(days) + " days ago"
    }
    
    public func dayBeginDate() -> Date {
        let calendar = Calendar.current
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
        components.setValue(0, for: .hour)
        components.setValue(0, for: .minute)
        components.setValue(0, for: .second)
        if let beginOfDayDate = calendar.date(from: components) {
            return beginOfDayDate
        }
        return self
    }
}
