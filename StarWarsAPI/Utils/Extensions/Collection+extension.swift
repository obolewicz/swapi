//
//  Collection+extension.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import Foundation

extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
