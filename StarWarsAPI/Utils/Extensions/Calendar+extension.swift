//
//  Calendar+extension.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import Foundation

extension Calendar {
    public func isDateThisYear(date: Date) -> Bool {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year], from: date)
        let componentsCurrent = calendar.dateComponents([.year], from: Date())
        return components.year == componentsCurrent.year
    }
}
