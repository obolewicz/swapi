//
//  BirthDateTransform.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import Foundation
import ObjectMapper

open class BirthDateTransform: TransformType {
    var formatter: DateFormatterHelper?
    
    public init(formatter: DateFormatterHelper? = nil) {
        if formatter == nil {
            self.formatter = DateFormatterHelper()
        }
    }
    
    open func transformFromJSON(_ value: Any?) -> Date? {
        if let timeString = value as? String {
            return self.formatter?.birthStringToDate(date: timeString)
        }
        return nil
    }
    
    open func transformToJSON(_ value: Date?) -> String? {
        if let date = value {
            return self.formatter?.birthDateToString(date: date)
        }
        return nil
    }
}

