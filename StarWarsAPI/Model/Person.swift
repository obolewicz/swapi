//
//  Person.swift
//  StarWarsAPI
//
//  Created by Marcin Obolewicz on 15.11.2017.
//  Copyright © 2017 pl.obol.swapi. All rights reserved.
//

import ObjectMapper

class Person: Mappable {
    
    var name: String = ""
    var height: String = ""
    var mass: String = ""
    var homeworld: String = ""
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        height <- map["height"]
        mass <- map["mass"]
        homeworld <- map["homeworld"]
    }
}
